class CreateTimePoints < ActiveRecord::Migration[5.1]
  def change
    create_table :time_points do |t|
      t.datetime :started_at
      t.datetime :finished_at
      t.references :task, foreign_key: true

      t.timestamps
    end
  end
end
