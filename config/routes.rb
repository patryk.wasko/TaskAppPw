Rails.application.routes.draw do
  root to: 'home#index'

  get '/sign_in', to: 'home#index'
  get '/sign_up', to: 'home#index'
  get '/tasks', to: 'home#index'
  get '/tasks/:id', to: 'home#index'
  get '/tasks/new', to: 'home#index'

  namespace :api do
    namespace :v1 do
      post '/sessions', to: 'sessions#create'
      delete '/sessions/sign_out', to: 'sessions#sign_out'
      get '/sessions/check_user', to: 'sessions#check_user'
     
      post '/registrations', to: 'registrations#create'
 
      resources :tasks, only: [ :index, :show, :create, :update, :destroy ]
      
      post '/time_points/start', to: 'time_points#start'
      patch '/time_points/finish', to: 'time_points#finish'
      delete '/time_points/:id', to: 'time_points#destroy'
    end
  end
end
