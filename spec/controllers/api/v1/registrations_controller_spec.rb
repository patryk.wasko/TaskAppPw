require 'rails_helper'

RSpec.describe Api::V1::RegistrationsController, type: :request do
  let(:user) { FactoryGirl.build :user }
  let(:password) { 'password123' }

  describe 'POST :create' do
    it 'return success for valid user params' do
      post '/api/v1/registrations', params: { email: user.email, password: password }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(200)
      expect(json['message']).to eq('Registration succesful')
    end
    it 'return error for invalid user params' do
      post '/api/v1/registrations', params: { email: '', password: '' }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(422)
      expect(json['message']).to eq("Email can't be blank, Password can't be blank")
    end
  end
end
