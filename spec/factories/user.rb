FactoryGirl.define do
  sequence :user_email do |n|
    "user#{n}@test.com"
  end

  factory :user do
    email { FactoryGirl.generate :user_email }
    password  "test123123"
    auth_token { Devise.friendly_token }
  end
end
