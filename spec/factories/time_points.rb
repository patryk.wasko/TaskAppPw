FactoryGirl.define do
  factory :time_point do
    started_at "2017-09-04 06:53:06"
    finished_at nil
    task { FactoryGirl.create :task }
  end
end
