class TimePoint < ApplicationRecord
  validates :task_id, presence: true
  belongs_to :task
  before_create :update_task_duration
  before_update :update_task_duration
  before_destroy :subtract_task_duration  

  def update_task_duration
    if self.finished_at_changed?
      time_difference = self.finished_at - self.started_at 
      self.task.increment!(:duration, time_difference)
      self.task.update(running: false)
    else
      self.task.update(running: true)
    end
  end
  
  def subtract_task_duration
    if self.finished_at
      time_difference = self.finished_at - self.started_at 
      self.task.decrement!(:duration, time_difference)
    end
  end
end
