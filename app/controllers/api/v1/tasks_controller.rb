class Api::V1::TasksController < Api::V1::BaseController
  protect_from_forgery with: :null_session
  before_action :authenticate_user
 
  def index
    render json: { tasks: current_user.tasks.order(:id) }, status: 200
  end

  def show
    render json: { task: task, time_points: task.time_points }, status: 200
  end

  def create
    task = current_user.tasks.create(task_params)
    if task.save
      render json: { task: task, message: 'Task created' }, status: 200
    else
      render json: { message: task.errors.full_messages.join(', ') }, status: 401
    end
  end
  
  def update
    begin
      task.update!(task_params)
      render json: { task: task, message: 'Task updated' }, status: 200
    rescue ActiveRecord::RecordInvalid => e
      render json: { message: e.message }, status: 401
    end
  end
  
  def destroy
    task.destroy
    render json: { message: 'Task deleted' }, status: 200
  end

  rescue_from ActiveRecord::RecordNotFound do |exception|
    render json: { message: 'Task not found' }, status: 404
  end

  private

  def task_params
    params.require(:task).permit(:title)
  end
  
  def task
    current_user.tasks.find(params[:id]) 
  end 
end
