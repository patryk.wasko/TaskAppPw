class Api::V1::RegistrationsController < Api::V1::BaseController
  protect_from_forgery with: :null_session

  def create
    email = params[:email].to_s.downcase.strip
    password = params[:password].to_s
    user = User.create(email: email, password: password, auth_token: Devise.friendly_token)
    if user.save
      render json: { message: 'Registration succesful' }, status: 200
    else
      render json: { message: user.errors.full_messages.join(', ') }, status: 422
    end
  end
end

