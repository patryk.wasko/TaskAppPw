import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import createMutationsSharer from 'vuex-shared-mutations'
import createPersistedState from 'vuex-persistedstate'

import { mutations } from './mutations'
import * as actions from './actions'

Vue.use(Vuex)

const state = {
  user: {},
  logged: false
}

export default new Vuex.Store ({
  state,
  mutations,
  actions,
  plugins: [createPersistedState(), createMutationsSharer({ predicate: ['SIGN_IN', 'SIGN_OUT'] })]
})
