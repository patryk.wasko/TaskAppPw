require 'rails_helper'

RSpec.describe Api::V1::SessionsController, type: :request do
  let(:user) { FactoryGirl.create :user }

  before :each do
    @password = 'password123'
    user.update!(password: @password)
  end

  describe 'POST :create' do
    it 'return user hash for valid user params' do
      expect(user.logged).to eq(false)
      post '/api/v1/sessions', params: { email: user.email, password: @password }
      json = JSON.parse(response.body)

      expect(response).to have_http_status(200)
      expect(json['message']).to eq('Signed In')
      expect(json['user']).to match({"id" => user.id, "email" => user.email, "auth_token" => /[\s\S]*/ })
      expect(json['user']['auth_token']).to_not be_nil
      expect(json['user']['id']).to eq(user.id)
      expect(json['user']['email']).to eq(user.email)
      user.reload
      expect(user.logged).to eq(true)
    end

    it 'return unauthorized for invalid user param' do
      post '/api/v1/sessions', params: { email: user.email, password: 'Wrong password' }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(401)
      expect(json['message']).to eq('Wrong email or password')
      expect(json['user']).to eq(nil)
    end

    it 'change logged flag to true if logged' do
      expect(user.logged).to eq(false)
      post '/api/v1/sessions', params: { email: user.email, password: @password }
      user.reload
      expect(user.logged).to eq(true)
    end
  end

  describe 'DELETE :sign_out' do
    it 'change logged flag to false for valid user credentials' do
      token = Devise.friendly_token
      user.update!(auth_token: token, logged: true)
      expect(user.logged).to eq(true)
      delete '/api/v1/sessions/sign_out', headers: { 'auth-token': token }
      user.reload
      expect(user.logged).to eq(false)
    end
  end

  describe 'GET :check_user'
    it 'return true if user credentials are correct and user has flag logged' do
      token = Devise.friendly_token
      user.update!(auth_token: token, logged: true)
      get '/api/v1/sessions/check_user', headers: { 'auth-token': token }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(200)
      expect(json['message']).to eq('Logged')
      expect(json['logged']).to eq(true)
    end

    it 'return false if user credentials are incorrect and user has flag logged' do
      token = Devise.friendly_token
      user.update!(auth_token: token, logged: true)
      get '/api/v1/sessions/check_user', headers: { 'auth-token': 'wrong_token' }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(401)
      expect(json['message']).to eq('Wrong session')
      expect(json['logged']).to eq(false)
    end

    it 'return false if user credentials are correct and user does not have flag logged' do
      token = Devise.friendly_token
      user.update!(auth_token: token, logged: false)
      get '/api/v1/sessions/check_user', headers: { 'auth-token': token }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(200)
      expect(json['message']).to eq('Session expired')
      expect(json['logged']).to eq(false)
    end

    it 'return false if user credentials are incorrect and user does not have flag logged' do
      token = Devise.friendly_token
      user.update!(auth_token: token, logged: false)
      get '/api/v1/sessions/check_user', headers: { 'auth-token': 'wrong_token' }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(401)
      expect(json['message']).to eq('Wrong session')
      expect(json['logged']).to eq(false)
    end
end
