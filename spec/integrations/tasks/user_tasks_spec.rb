require 'rails_helper'

RSpec.feature 'User tasks', type: :feature do
  let(:task) { FactoryGirl.build :task }
  let(:other_task) { FactoryGirl.create :task }
 
  before :each do
    visit '/'
  end 

  scenario 'Signed user visit tasks' do
    user = login_as_user
    task.update(user: user)
    within('.left_panel') do
      expect(page).to have_css('#tasks_link')
      page.find('#tasks_link').click
    end
    expect(page).to have_current_path('/tasks')
    within('#tasks_content') do
      expect(page).to have_content(task.title)
      expect(page).to have_no_content(other_task.title)
    end
  end

  scenario 'Usigned user visit tasks via link' do
    within('.left_panel') do
      expect(page).to have_css('#tasks_link')
      page.find('#tasks_link').click
    end
    expect(page).to have_current_path('/sign_in')
    within('.flash_msg') do
      expect(page).to have_content('You must be logged to visit this page')
    end
  end

  scenario 'Usigned user visit tasks directly' do
    visit '/tasks'
    expect(page).to have_current_path('/sign_in')
    within('.flash_msg') do
      expect(page).to have_content('You must be logged to visit this page')
    end
  end

  scenario 'Signed user delete task' do
    user = login_as_user
    task.update(user: user)
    within('.left_panel') do
      expect(page).to have_css('#tasks_link')
      page.find('#tasks_link').click
    end
    expect(page).to have_current_path('/tasks')
    within("#task_#{task.id}") do
      expect{ page.find('.btn', text: 'Delete').click }.to change{ Task.count }.by(-1)
    end
    within('#tasks_content') do
      expect(page).to have_no_content(task.title)
    end
    within('.flash_msg') do
      expect(page).to have_content('Task deleted')
    end
  end
  
  scenario 'Signed user create task with valid params' do
    user = login_as_user
    within('.left_panel') do
      page.find('#tasks_link').click
    end
    within('#tasks_content') do
      expect(page).to have_no_content(task.title)
      page.find('#new_task_link').click
    end
    expect(page).to have_current_path('/tasks/new')
    within('.new_task_form') do
      page.find('#task_title').set task.title
      expect{ page.find('#create_task_btn').click }.to change{Task.count}.by(1)
    end
    expect(page).to have_current_path('/tasks')
    within('#tasks_content') do
      expect(page).to have_content(task.title)
    end
  end

  scenario 'Signed user create task with valid params' do
    user = login_as_user
    within('.left_panel') do
      page.find('#tasks_link').click
    end
    within('#tasks_content') do
      expect(page).to have_no_content(task.title)
      page.find('#new_task_link').click
    end
    expect(page).to have_current_path('/tasks/new')
    within('.new_task_form') do
      page.find('#task_title').set ''
      expect{ page.find('#create_task_btn').click }.to_not change{Task.count}
    end
    expect(page).to have_current_path('/tasks/new')
    within('.flash_msg') do
      expect(page).to have_content("Title can't be blank")
    end
  end
 
  scenario 'Unsigned user visit create task path' do 
    visit('/tasks/new')
    expect(page).to have_current_path('/')
    within('.flash_msg') do
      expect(page).to have_content('You must be logged to visit this page')
    end
  end

  scenario 'User visit task path' do
    user = login_as_user
    task.update(user: user)
    within('.left_panel') do
      page.find('#tasks_link').click
    end
    within("#task_#{task.id}") do
      page.find('.task_link').click
    end
    expect(page).to have_current_path("/tasks/#{task.id}")
    expect(page).to have_content(task.title)
  end

  scenario 'User try to visit other user task' do
    login_as_user
    other_task
    visit "/tasks/#{other_task.id}" 
    expect(page).to have_current_path("/tasks")
    within('.flash_msg') do
      expect(page).to have_content('Task not found')
    end
  end

  scenario 'User edit task' do
    new_title = 'new_title'
    user = login_as_user
    task.update(user: user)
    within('.left_panel') do
      page.find('#tasks_link').click
    end
    within("#task_#{task.id}") do
      page.find('.task_link').click
    end
    expect(page).to have_current_path("/tasks/#{task.id}")
    expect(page).to have_content(task.title)
    within('#task_content') do
      page.find('.edit_task_btn').click
      page.find('#task_title').set new_title
      page.find('.update_task_btn').click
    end
    within('.flash_msg') do
      expect(page).to have_content('Task updated')
    end
    within('#task_content') do
      expect(page).to have_content(new_title)
      expect(page).to have_css('.edit_task_btn')
      expect(page).to_not have_css('.update_task_btn')
    end
  end
end
