class Api::V1::SessionsController < Api::V1::BaseController
  protect_from_forgery with: :null_session
  before_action :authenticate_user, only: [:sign_out]

  def create
    email = params[:email].to_s.downcase.strip
    password = params[:password].to_s
    user = User.find_by_email(email)
    if user && user.email == email && user.valid_password?(password)
      user.update!(auth_token: Devise.friendly_token, logged: true)
      render json: { user: {
                             id: user.id,
                             email: user.email,
                             auth_token: user.auth_token
                           },
                     message: 'Signed In'
                   }
    else
      render json: { message: 'Wrong email or password' }, status: 401
    end
  end
 
  def sign_out
    current_user.update!(logged: false)
    render json: { message: 'Signed Out' }, status: 200
  end
  
  def check_user
    if current_user && current_user.logged
      render json: { message: 'Logged', logged: true }, status: 200
    elsif current_user
      render json: { message: 'Session expired', logged: false }, status: 200
    else
      render json: { message: 'Wrong session', logged: false }, status: 401
    end
  end
end

