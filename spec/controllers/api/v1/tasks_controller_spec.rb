require 'rails_helper'

RSpec.describe Api::V1::TasksController, type: :request do
  let(:user) { FactoryGirl.create :user }
  let(:task) { FactoryGirl.create :task }
  let(:task_2) { FactoryGirl.create :task }
  let(:task_3) { FactoryGirl.create :task }
 
 describe 'GET :index' do
    before :each do
      task.update(user: user)
      task_2.update(user: user)
      task_3.save
    end

    it 'return user tasks' do
      get '/api/v1/tasks', headers: { 'auth-token': user.auth_token }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(200)
      expect(json['tasks'].to_json).to eq(user.tasks.to_json)
    end
    
    it 'return auth error for invalid auth-token' do 
      get '/api/v1/tasks', headers: { 'auth-token': 'wrong_token' }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(401)
      expect(json['message']).to eq("Wrong session")
    end
  end
 
  describe 'GET :show' do
    it 'return task for valid user' do 
      task.update(user: user)
      get "/api/v1/tasks/#{task.id}", headers: { 'auth-token': user.auth_token }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(200)
      expect(json['task']).to match({"id" => task.id, 
                                     "running" => task.running,
                                     "title" => task.title, 
                                     "user_id" => task.user_id,
                                     "duration" => task.duration,
                                     "created_at" => task.created_at.as_json,
                                     "updated_at" => task.updated_at.as_json })
    end

    it 'return task for valid user' do 
      task
      get "/api/v1/tasks/#{task.id}", headers: { 'auth-token': user.auth_token }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(404)
      expect(json['message']).to eq('Task not found')
    end
  end
 
  describe 'POST :create' do
    before :each do
      task
    end

    it 'create task with valid params' do
      expect { post '/api/v1/tasks', params: { task: { title: task.title }}, headers: { 'auth-token': user.auth_token }}.to change{ Task.count }.by(1)
      json = JSON.parse(response.body)
      expect(response).to have_http_status(200)
      expect(json['message']).to eq('Task created')
      last_task = Task.last
      expect(json['task']).to match({"id" => last_task.id, 
                                     "running" => task.running,
                                     "title" => last_task.title, 
                                     "user_id" => last_task.user_id, 
                                     "duration" => last_task.duration,
                                     "created_at" => last_task.created_at.as_json, 
                                     "updated_at" => last_task.updated_at.as_json })
    end
   
    it 'create task with invalid params' do
      expect { post '/api/v1/tasks', params: { task: { title: '' }}, headers: { 'auth-token': user.auth_token }}.to_not change{ Task.count }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(401)
      expect(json['message']).to eq("Title can't be blank")
    end

    it 'create task with invalid token' do
      expect{ post '/api/v1/tasks', params: {task: { title: task.title }}, headers: { 'auth-token': 'wrong_token' }}.to_not change{ Task.count }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(401)
      expect(json['message']).to eq("Wrong session")
    end
  end
  
  describe 'PATCH :update' do
    it 'update task with valid params and user permissions' do
      new_title = 'new title'
      task.update(user: user)
      patch "/api/v1/tasks/#{task.id}", params: { task: { title: new_title }}, headers: { 'auth-token': user.auth_token }
      task.reload
      json = JSON.parse(response.body)
      expect(response).to have_http_status(200)
      expect(json['message']).to eq('Task updated')
      expect(json['task']).to match({"id" => task.id,
                                     "running" => task.running,
                                     "title" => new_title,
                                     "user_id" => task.user_id,
                                     "duration" => task.duration,
                                     "created_at" => task.created_at.as_json,
                                     "updated_at" => task.updated_at.as_json })
    end

    it 'update task with valid params and without user permissions' do
      new_title = 'new title'
      patch "/api/v1/tasks/#{task.id}", params: { task: { title: new_title }}, headers: { 'auth-token': user.auth_token }
      task.reload
      json = JSON.parse(response.body)
      expect(response).to have_http_status(404)
      expect(task.title).to_not eq(new_title)
    end

    it 'update task with invalid params and with user permissions' do
      task.update(user: user)
      patch "/api/v1/tasks/#{task.id}", params: { task: { title: '' }}, headers: { 'auth-token': user.auth_token }
      task.reload
      json = JSON.parse(response.body)
      expect(response).to have_http_status(401)
      expect(json['message']).to eq("Validation failed: Title can't be blank")
    end
  end 
  
  describe 'DELETE :destroy' do
    before :each do 
      task
    end
    it 'delete task with user permissions' do
      task.update(user: user)
      expect{ delete "/api/v1/tasks/#{task.id}", headers: { 'auth-token': user.auth_token }}.to change{ Task.count }.by(-1)
      json = JSON.parse(response.body)
      expect(response).to have_http_status(200)
      expect(json['message']).to eq("Task deleted")
    end
  
    it 'delete task without user permissions' do
      expect{ delete "/api/v1/tasks/#{task.id}", headers: { 'auth-token': user.auth_token }}.to_not change{ Task.count }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(404)
      expect(json['message']).to eq("Task not found")
    end
  end
end
