require 'rails_helper'

RSpec.describe Api::V1::TimePointsController, type: :request do
  let(:user) { FactoryGirl.create :user }
  let(:task) { FactoryGirl.create :task }
  let(:time_point) { FactoryGirl.create :time_point }
  
  describe 'POST :create' do
    it 'create time point with permissions  and valid params' do
      task.update(user: user)      
      expect { post '/api/v1/time_points/start', params: { time_point: { task_id: task.id }}, headers: { 'auth-token': user.auth_token }}.to change{ TimePoint.count }.by(1)
      json = JSON.parse(response.body)
      expect(response).to have_http_status(200)
      expect(json['message']).to eq('Time point added')
    end

    it 'create time point with permissions  and invalid params' do
      task.update(user: user)      
      expect { post '/api/v1/time_points/start', params: { time_point: { task_id: '' }}, headers: { 'auth-token': user.auth_token }}.to_not change{ TimePoint.count }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(404)
      expect(json['message']).to eq("Can't find task for current user")
    end
  
    it 'create time point without permissions and invalid params' do
      expect { post '/api/v1/time_points/start', params: { time_point: { task_id: '' }}, headers: { 'auth-token': user.auth_token }}.to_not change{ TimePoint.count }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(404)
      expect(json['message']).to eq("Can't find task for current user")
    end
  
    it 'create time point without permissions  and valid params' do
      expect { post '/api/v1/time_points/start', params: { time_point: { task_id: task.id }}, headers: { 'auth-token': user.auth_token }}.to_not change{ TimePoint.count }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(404)
      expect(json['message']).to eq("Can't find task for current user")
    end
  
    it 'create time point without login and valid params' do
      expect { post '/api/v1/time_points/start', params: { time_point: { task_id: task.id }}, headers: { 'auth-token': '' }}.to_not change{ TimePoint.count }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(401)
      expect(json['message']).to eq("Wrong session")
    end

    it 'create time point without login and invalid params' do
      expect { post '/api/v1/time_points/start', params: { time_point: { task_id: '' }}, headers: { 'auth-token': '' }}.to_not change{ TimePoint.count }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(401)
      expect(json['message']).to eq("Wrong session")
    end

    it 'create time point for task with already started time point' do
      task.time_points.create(started_at: Time.now)
      task.update(user: user)      
      expect { post '/api/v1/time_points/start', params: { time_point: { task_id: task.id }}, headers: { 'auth-token': user.auth_token }}.to_not change{ TimePoint.count }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(400)
      expect(json['message']).to eq('Task is still open')
    end
  end

  describe 'PATCH :update' do
    it 'update time point with valid params and user permissions' do
      task.update(user: user)
      time_point.update(task: task)
      expect(time_point.finished_at).to eq(nil)
      patch "/api/v1/time_points/finish", params: { time_point: { task_id: task.id }}, headers: { 'auth-token': user.auth_token }
      time_point.reload
      task.reload
      time_point_time = time_point.finished_at - time_point.started_at
      expect(task.duration.round(5)).to eq(time_point_time.round(5))
      expect(time_point.finished_at).to_not eq(nil)
      json = JSON.parse(response.body)
      expect(response).to have_http_status(200)
      expect(json['message']).to eq('Time point updated')
    end

    it 'update time point with valid params and user permissions but without active timepoint' do
      task.update(user: user)
      time_point.update(task: task)
      old_finished_time = Time.now
      time_point.update(finished_at: old_finished_time)
      expect(time_point.finished_at).to_not eq(nil)
      patch "/api/v1/time_points/finish", params: { time_point: { task_id: task.id }}, headers: { 'auth-token': user.auth_token }
      json = JSON.parse(response.body)
      expect(json['message']).to eq('Task is not started')
    end
  end
  describe 'DELETE :destroy' do
    it 'delete unfinished time point with user permissions' do
      task_duration = task.duration
      task.update(user: user)
      time_point.update(task: task)
      delete "/api/v1/time_points/#{time_point.id}", headers: { 'auth-token': user.auth_token }
      task.reload
      expect(task.duration).to eq(task_duration)
      json = JSON.parse(response.body)
      expect(response).to have_http_status(200)
      expect(json['message']).to eq('Time point deleted')
    end

    it 'delete finished time point with user permissions' do
      task_duration = task.duration
      task.update(user: user)
      time_point.update(task: task)
      time_point.update(finished_at: Time.now)
      expect(task.duration.round(5)).to_not eq(task_duration.round(5))
      delete "/api/v1/time_points/#{time_point.id}", headers: { 'auth-token': user.auth_token }
      task.reload
      expect(task.duration.round(5)).to eq(task_duration.round(5))
      json = JSON.parse(response.body)
      expect(response).to have_http_status(200)
      expect(json['message']).to eq('Time point deleted')
    end

    it 'delete time point without user permissions' do
      time_point.update(task: task)
      delete "/api/v1/time_points/#{time_point.id}", headers: { 'auth-token': user.auth_token }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(400)
      expect(json['message']).to eq('You have no permission to delete this time point')
    end
  end
end
