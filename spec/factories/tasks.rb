FactoryGirl.define do
  sequence :user_task do |n|
    "task#{n}"
  end

  factory :task do
    title { FactoryGirl.generate :user_task }
    user { FactoryGirl.create :user }
  end
end
