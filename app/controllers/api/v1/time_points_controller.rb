class Api::V1::TimePointsController < Api::V1::BaseController
  protect_from_forgery with: :null_session
  before_action :authenticate_user
  before_action :check_task_before_start, only: [:start]
  before_action :check_task_before_finish, only: [:finish]

  def start
    task.time_points.create(started_at: Time.now)
    render json: { message: 'Time point added' }, status: 200
  end

  def finish
    task.time_points.last.update(finished_at: Time.now)
    render json: { message: 'Time point updated' }, status: 200
  end
 
  def destroy
    time_point = TimePoint.find(params[:id])
    if time_point.task.user == current_user
      time_point.destroy
      render json: { message: 'Time point deleted' }, status: 200
    else
      render json: { message: 'You have no permission to delete this time point' }, status: 400
    end
  end
  
  class NotStartedTask < StandardError
  end

  class NotFinishedTask < StandardError
  end

  private

  def check_task_before_start
    if task.time_points.last 
      unless task.time_points.last.finished_at
        raise NotFinishedTask
      end
    end
  end

  def check_task_before_finish
    if task.time_points.last &&  task.time_points.last.finished_at
      raise NotStartedTask 
    end
  end
  
  def task
    current_user.tasks.find(params[:time_point][:task_id])
  end

  rescue_from ActiveRecord::RecordNotFound do |exception|
    render json: { message: "Can't find task for current user" }, status: 404
  end

  rescue_from NotStartedTask do |exception|
    render json: { message: "Task is not started" }, status: 400
  end

  rescue_from NotFinishedTask do |exception|
    render json: { message: "Task is still open" }, status: 400
  end
end
