require 'rails_helper'

RSpec.feature 'User login', type: :feature do
  let(:user) { FactoryGirl.create :user }

  scenario 'User login with correct user params' do
    visit '/'
    within('.left_panel') do
      page.find('#sign_in_link').click
    end
    within('.sign_in_form') do
      page.find('#user_email').set  user.email
      page.find('#user_password').set user.password
      page.find('#sign_in_btn').click
    end
    within('.left_panel') do
      expect(page).to have_content(user.email)
      expect(page).to have_content('Sign Out')
    end
    expect(page).to have_current_path('/')
    within('.flash_msg') do
      expect(page).to have_content('Signed In')
    end
  end

  scenario 'User login with incorrect user params' do
    visit '/'
    within('.left_panel') do
      page.find('#sign_in_link').click
    end
    within('.sign_in_form') do
      page.find('#user_email').set  user.email
      page.find('#user_password').set 'Wrong passowrd'
      page.find('#sign_in_btn').click
    end
    within('.left_panel') do
      expect(page).to have_no_content(user.email)
      expect(page).to have_no_content('Sign Out')
    end
    expect(page).to have_content('Wrong email or password')
    expect(page).to have_current_path('/sign_in')
    within('.flash_msg') do
      expect(page).to have_content('Wrong email or password')
    end
  end

  scenario 'User record destroyed during the session' do
    visit '/'
    within('.left_panel') do
      page.find('#sign_in_link').click
    end
    within('.sign_in_form') do
      page.find('#user_email').set  user.email
      page.find('#user_password').set user.password
      page.find('#sign_in_btn').click
        end
    user.delete
    visit '/'
    within('.left_panel') do
      expect(page).to have_no_content(user.email)
      expect(page).to have_no_content('Sign Out')
      expect(page).to have_css('#sign_in_link')
    end 
    within('.flash_msg') do
      expect(page).to have_content('Wrong session')
    end
  end

  scenario 'User signed out during other session' do
    visit '/'
    within('.left_panel') do
      page.find('#sign_in_link').click
    end
    within('.sign_in_form') do
      page.find('#user_email').set user.email
      page.find('#user_password').set user.password
      page.find('#sign_in_btn').click
    end
    user.reload
    user.update(logged: false)
    visit '/'
    within('.left_panel') do
      expect(page).to have_no_content(user.email)
      expect(page).to have_no_content('Sign Out')
      expect(page).to have_css('#sign_in_link')
    end 
    within('.flash_msg') do
      expect(page).to have_content('Session expired')
    end
  end

  scenario 'User signout beeing on page that required user' do
    visit '/'
    login_as_user
    within('.left_panel') do
      page.find('#tasks_link').click
    end
    expect(page).to have_current_path('/tasks')
    within('.left_panel') do
      page.find('#sign_out_btn').click
    end
    expect(page).to have_current_path('/')
  end

  scenario 'User signout' do
    visit '/'
    user = login_as_user
    within('.left_panel') do
      expect(page).to have_content(user.email)
      page.find('#sign_out_btn').click
    end
    expect(page).to have_current_path('/')
    expect(page).to have_no_content(user.email)
  end
end
