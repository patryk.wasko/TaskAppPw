class Api::V1::BaseController < ApplicationController
  respond_to :json

  def current_user
    User.find_by(auth_token: request.headers['auth-token'])
  end

  def authenticate_user
    raise AuthenticationError unless current_user
  end

  class AuthenticationError < StandardError
  end

  rescue_from AuthenticationError do |exception|
    render json: { message: 'Wrong session' }, status: 401
  end
end
