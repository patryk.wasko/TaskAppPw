/* eslint no-console: 0 */
// Run this example by adding <%= javascript_pack_tag 'hello_vue' %> and
// <%= stylesheet_pack_tag 'hello_vue' %> to the head of your layout file,
// like app/views/layouts/application.html.erb.
// All it does is render <div>Hello Vue</div> at the bottom of the page.

import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import store from './store/'

import App from './app.vue'
import SignIn from './components/signIn.vue'
import SignUp from './components/signUp.vue'
import Dashboard from './components/dashboard.vue'
import LeftPanel from './components/leftPanel.vue'
import topPanel from './components/topPanel.vue'
import mainPanel from './components/mainPanel.vue'
import flashMsg from './components/flashMsg.vue'
import TasksPanel from './components/tasks/tasksPanel.vue'
import NewTask from './components/tasks/newTask.vue'
import Task from './components/tasks/task.vue'
import TaskActionBtn from './components/tasks/taskActionBtn.vue'
import Timer from './components/timer.vue'

Vue.use(VueRouter)
Vue.use(VueResource)

Vue.component('left-panel', LeftPanel)
Vue.component('top-panel', topPanel)
Vue.component('main-panel', mainPanel)
Vue.component('flash-msg', flashMsg)
Vue.component('tasks-panel', TasksPanel)
Vue.component('task-action-btn', TaskActionBtn)
Vue.component('timer', Timer)

const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', component: Dashboard },
    { path: '/sign_in', component: SignIn },
    { path: '/sign_up', component: SignUp },
    { path: '/tasks', component: TasksPanel },
    { path: '/tasks/new', component: NewTask },
    { path: '/tasks/:id', component: Task }
  ]
})


document.addEventListener('DOMContentLoaded', () => {
  document.body.appendChild(document.createElement('vue'))
  const app = new Vue({
    el: 'vue',
    router,
    template: '<App/>',
    store,
    components: {
      App
    },
    render: h => h(App)
  })
})
