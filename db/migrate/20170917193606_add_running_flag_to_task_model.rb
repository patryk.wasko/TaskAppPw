class AddRunningFlagToTaskModel < ActiveRecord::Migration[5.1]
  def change
    add_column :tasks, :running, :boolean, default: false
  end
end
