module LoginSupport

  def login_as_user
    user = FactoryGirl.create :user
    visit '/'
    within('.left_panel') do
      page.find('#sign_in_link').click
    end
    within('.sign_in_form') do
      page.find('#user_email').set  user.email
      page.find('#user_password').set user.password
      page.find('#sign_in_btn').click
    end
    user
  end
end

