# README
## Task App:

Simple SPA application that help you measure time spend on tasks.


## To run dev server:

    foreman start -f Procfile.dev

## Main lib:
* Ruby on Rails 5.1.3
* Vue 2.4.2
* Postgresq
* Webpack
* Webpacker
* RSpec
* Capybara

## App avaible on:
[TaskApp](https://taskapp-pw.herokuapp.com/)
