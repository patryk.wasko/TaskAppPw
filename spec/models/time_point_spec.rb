require 'rails_helper'

RSpec.describe TimePoint, type: :model do
  it { is_expected.to have_db_column(:started_at) }
  it { is_expected.to have_db_column(:finished_at) }
  it { is_expected.to have_db_column(:task_id) }

  it { is_expected.to validate_presence_of(:task_id) }

  it { is_expected.to belong_to(:task) }
end
