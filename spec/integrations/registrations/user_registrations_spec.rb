require 'rails_helper'

RSpec.feature 'User login', type: :feature do
  let(:user) { FactoryGirl.build :user }

  background do
    visit '/'
    within('.left_panel') do
      page.find('#sign_up_link').click
    end
  end

  scenario 'User register account with correct user params' do
    within('.sign_up_form') do
      page.find('#user_email').set  user.email
      page.find('#user_password').set user.password
      page.find('#user_password_confirmation').set user.password
      page.find('#sign_up_btn').click
    end

    within('.flash_msg') do
      expect(page).to have_content('Signed In')
   end

    within('.left_panel') do
      expect(page).to have_content(user.email)
      expect(page).to have_content('Sign Out')
    end
    expect(page).to have_current_path('/')
  end

  scenario 'User register account with wrong password confirmation user params ' do
    within('.sign_up_form') do
      page.find('#user_email').set  user.email
      page.find('#user_password').set user.password
      page.find('#user_password_confirmation').set 'wrong password'
      page.find('#sign_up_btn').click
    end
    within('.sign_up_form') do
      expect(page).to have_content('Passwords must be the same!')
    end
    expect(page).to have_current_path('/sign_up')
  end

  scenario 'User register account without params user params ' do
    within('.sign_up_form') do
      page.find('#user_email').set  ''
      page.find('#user_password').set ''
      page.find('#user_password_confirmation').set ''
      page.find('#sign_up_btn').click
    end
    within('.flash_msg') do
      expect(page).to have_content("Email can't be blank, Password can't be blank")
    end
    expect(page).to have_current_path('/sign_up')
  end
end
